<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
/*
  |--------------------------------------------------------------------------
  | API Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register API routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | is assigned the "api" middleware group. Enjoy building your API!
  |
 */

Route::group([
    'prefix' => 'auth'
], function () {
    Route::post('login', 'API\UserController@login');


    Route::post('loginGoogle', 'API\UserController@loginGoogle');
    Route::post('signup', 'API\UserController@signup');
    Route::post('signupClient', 'API\UserController@signupClient');
    Route::get('auth/{provider}', 'API\UserController@redirectToProvider');
    Route::get('auth/{provider}/callback', 'API\UserController@hanbleProviderCallback');
    Route::post('forgot-password', 'Auth\ForgotPasswordController@forgot');
    Route::post('reset-password', 'Auth\ForgotPasswordController@reset');
    Route::post('get-code-recovery', 'Auth\ForgotPasswordController@get_code_recovery');
    Route::post('recovery-password', 'Auth\ForgotPasswordController@reset_recovery');

    Route::group([
        'middleware' => 'auth:api'
    ], function () {
        Route::get('logout', 'API\UserController@logout');
        Route::post('logout', 'API\UserController@logout');


        Route::get('user', 'API\UserController@user');
        Route::post('getUser', 'API\UserController@getUser');
    });
});

Route::group([
    'prefix' => 'evaluation'
], function () {
    Route::group([
        'middleware' => 'auth:api',
    ], function () {
        Route::get('getEvaluations', 'API\EvaluationController@get_evaluations');
        Route::post('getEvaluationsByEvaluatorUser', 'API\EvaluationController@get_evaluations_by_evaluator_user');
        Route::post('getEvaluationsByEvaluatedUser', 'API\EvaluationController@get_evaluations_by_evaluated_user');
        Route::post('saveEvaluation', 'API\EvaluationController@save_evaluation');
        Route::post('saveUserResponseEvaluation', 'API\EvaluationController@save_user_response_evaluation');
        Route::post('assignEvaluationToUser', 'API\EvaluationController@assign_evaluation_to_user');
        Route::post('sendQualificationToUser', 'API\EvaluationController@send_qualification_to_user');
        Route::post('getAllEvaluationsAndUsers', 'API\EvaluationController@get_all_evaluations');
    });
});

Route::group([
    'prefix' => 'user'
], function () {
    Route::group([
        'middleware' => 'auth:api',
    ], function () {
        Route::get('getUser', 'API\UserController@get_users');
        
    });
});


Route::group([
    'prefix' => 'front'
], function () {
    Route::post('insertEvaluation', 'API\EvaluationController@insert_evaluation');
});

