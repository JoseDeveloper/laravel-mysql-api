<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Evaluation extends Model
{
    protected $table = 'evaluations';
    protected $fillable = [
        'id',
        'name',
        'description',
        'score'
    ];

    public function get_evaluations()
    {

        $result = Evaluation::select('evaluations.*')
        ->get();

        foreach($result as $ev){
            $questions = Question::where('evaluation_id',$ev->id)->get();

            foreach($questions as $q){ 
                $q->answers_decode = json_decode($q->answers);
            }

            $ev->questions = $questions;
        }
        return $result;
    }

    public function get_all_evluations()
    {
        $result = EvaluationUser::leftJoin('evaluations', 'evaluations.id', '=', 'evaluation_user.evaluation_id')
            ->leftJoin('users', 'evaluation_user.evaluated_user_id', '=', 'users.id')
            ->select(
                'evaluation_user.*',
                'evaluation_user.user_score as userScore',
                'evaluations.name as evaluationName',
                'evaluations.score as evaluationScore',
                'evaluations.created_at as evaluationDate',
                'users.name as userName'
            )
            ->get();

        foreach ($result as $ev) {
            $questions = Question::where('evaluation_id', $ev->evaluation_id)->get();

            foreach ($questions as $q) {
                $q->answers_decode = json_decode($q->answers);
                $user_answers = UserAnswer::where('question_id', $q->id)->first();
                $user_answers['user_answers'] = json_decode($user_answers['user_answers']);
                $q->user_answers = $user_answers;
            }

            $ev->questions = $questions;
        }

        return $result;
    }

    public function insert_evaluation($evaluation_name)
    {

        $objectSave = [
            'name' => $evaluation_name,
            'score' => '10'
        ];

        $rowCreated = Evaluation::create($objectSave);
        return $rowCreated->id;
    }

    public function get_only_evaluations()
    {

        $result = Evaluation::select('evaluations.*')->get();
        return $result;
    }

    

   
}
