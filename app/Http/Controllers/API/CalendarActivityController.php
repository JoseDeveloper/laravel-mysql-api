<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\CalendarActivity;
use App\Models\Service;
use App\Models\Passe;
use App\Models\PasseUser;
use App\Models\PasseUserActivity;
use Carbon\Carbon;

class CalendarActivityController extends Controller
{
    public function get_calendar_activities(Request $request)
    {

        $page = $request->has('page') ? $request->get('page') : 1;
        $limit = $request->has('limit') ? $request->get('limit') : 10;

        $calendar_activity = new CalendarActivity;
        $calendar_activities = $calendar_activity->get_calendar_activities($page, $limit);

        $service = new Service;
        $services = $service->get_services();

        return response()->json([
            'data' => $calendar_activities,
            'services' => $services
        ]);
    }

    public function get_calendar_activities_by_date(Request $request)
    {

        $page = $request->has('page') ? $request->get('page') : 1;
        $limit = $request->has('limit') ? $request->get('limit') : 10;
        $limit = $request->has('limit') ? $request->get('limit') : 10;
        $user_id = $request->input('user_id');
        $service_selected = $request->input('service_selected');

        $date = ($request->input('date') != 'null') ?  Carbon::parse($request->input('date')) : Carbon::now()->format('Y-m-d');
        $today = Carbon::now();

        $calendar_activity = new CalendarActivity;
        $calendar_activities = ($service_selected != 0) ? $calendar_activity->get_calendar_activities_by_date($page, $limit, $service_selected, $date) : null;

        $passe_user = new PasseUser;
        $passes_user = ($user_id != 'null') ? $passe_user->get_user_passes_calendar($user_id, $page, $limit) : null;


        $service = new Service;
        $services = $service->get_services_alphabet_order();

        return response()->json([
            'data' => $calendar_activities,
            'userPasses' => $passes_user,
            'services' => $services,
            'today' => $today,
            'date' => $date,
            'datdex' => $request->input('date')
        ]);
    }

    public function get_calendar_activities_by_date_and_gym_branch(Request $request)
    {

        $page = $request->has('page') ? $request->get('page') : 1;
        $limit = $request->has('limit') ? $request->get('limit') : 10;
        $limit = $request->has('limit') ? $request->get('limit') : 10;
        $user_id = $request->input('user_id');
        $service_selected = $request->input('service_selected');
        $branch_id = $request->input('branch_id');

        $date = ($request->input('date') != 'null') ?  Carbon::parse($request->input('date')) : Carbon::now()->format('Y-m-d');
        $today = Carbon::now();

        $calendar_activity = new CalendarActivity;
        $calendar_activities = ($service_selected != 0) ? $calendar_activity->get_calendar_activities_by_date_and_gym_branch($page, $limit, $branch_id, $service_selected, $date) : null;

        $passe_user = new PasseUser;
        $passes_user = ($user_id != 'null') ? $passe_user->get_user_passes_calendar($user_id, $page, $limit) : null;


        $service = new Service;
        $services = $service->get_services_alphabet_order();

        return response()->json([
            'data' => $calendar_activities,
            'userPasses' => $passes_user,
            'services' => $services,
            'today' => $today,
            'date' => $date,
            'datdex' => $request->input('date')
        ]);
    }

    public function reserv_activity(Request $request)
    {

        $passe_user_id = $request->input('passe_user_id');
        $activity_id = $request->input('activity_id');

        $passe = new Passe;
        $passe_user = new PasseUser;
        $passe_user_ativity = new PasseUserActivity;
        $calendar_activity = new CalendarActivity;

        $user_passe = $passe_user->set_use_passe($passe_user_id);
        $passe = $passe->set_passe($user_passe->passe_id);
        $update = $calendar_activity->set_avaible($activity_id);
        
        // 'calendar_activities.space_avaible as spaceAvaible'

        $object_save = [
            'passe_user_id' => $passe_user_id,
            'calendar_activity_id' => $activity_id
        ];

        $response = $passe_user_ativity->create_passe_user_activity($object_save);

        return response()->json([
            'data' => $response,
            //'services' => $services
        ]);
    }



    public function get_calendar_activities_by_branch(Request $request)
    {

        $page = $request->has('page') ? $request->get('page') : 1;
        $limit = $request->has('limit') ? $request->get('limit') : 10;
        $branch_id = $request->get('branch_id');

        $calendar_activity = new CalendarActivity;
        $passe_user_ativity = new PasseUserActivity;
        $calendar_activities = $calendar_activity->get_calendar_activities_by_branch($page, $limit, $branch_id);
        $passe_user_activities = $passe_user_ativity->get_passes_user_activity_by_gym_branch($branch_id);

        $service = new Service;
        $services = $service->get_services();

        return response()->json([
            'data' => $calendar_activities,
            'services' => $services,
            'passe_user_activities' => $passe_user_activities,
            'pass_id' => $branch_id
        ]);
    }



    public function save_calendar_activity(Request $request)
    {
        $calendar_activity = new CalendarActivity;
        $id = $request->input('id');
        $branch_id = $request->input('branch_id');
        $service_id = $request->input('service_id');
        $description = $request->input('description');
        $title = $request->input('title');
        $start = $request->input('start');
        $end = $request->input('end');
        $start_lesson = $request->input('start_lesson');
        $end_lesson = $request->input('end_lesson');
        $n_space = $request->input('n_space');
        $space_avaible = $request->input('space_avaible');
        
        $objectSave = [
            'service_id' => $service_id,
            'branch_id' => $branch_id,
            'title' => $title,
            'description' => $description,
            'n_space' => $n_space,
            'space_avaible' => $space_avaible,
            'start' => Carbon::parse($start),
            'end' => Carbon::parse($end),
            'start_lesson' => Carbon::parse($start_lesson),
            'end_lesson' => Carbon::parse($end_lesson),
        ];

        if ($id != 'null') {

            $response = $calendar_activity->update_calendar_activity($id, $objectSave);
        } else {
            $response_id = $calendar_activity->create_calendar_activity($objectSave);
        }

        $page = $request->has('page') ? $request->get('page') : 1;
        $limit = $request->has('limit') ? $request->get('limit') : 10;
        $data = $calendar_activity->get_calendar_activities_by_branch($page, $limit, $branch_id);

        return response()->json([
            "error" => "",
            "data" => $data,
            "starend" => Carbon::parse($start)
        ]);
    }

    public function delete_calendar_activity(Request $request)
    {
        $calendar_activity = new CalendarActivity;
        $id = $request->input('id');
        $response = $calendar_activity->delete_calendar_activity($id);

        $page = $request->has('page') ? $request->get('page') : 1;
        $limit = $request->has('limit') ? $request->get('limit') : 10;
        $data = $calendar_activity->get_calendar_activities($page, $limit);

        return response()->json([
            "error" => "",
            "response" => $response,
            "data" => $data
        ]);
    }

    public function set_show_calendar_activity(Request $request)
    {
        $calendar_activity = new CalendarActivity;
        $id = $request->input('id');
        $show_value = $request->input('show_value');
        $response = $calendar_activity->set_show_calendar_activity($id, $show_value);

        $page = $request->has('page') ? $request->get('page') : 1;
        $limit = $request->has('limit') ? $request->get('limit') : 10;
        $data = $calendar_activity->get_calendar_activities($page, $limit);

        return response()->json([
            "error" => "",
            "response" => $response,
            "data" => $data
        ]);
    }

    public function set_assistance(Request $request)
    {
        $passe_user_activity = new PasseUserActivity;
        $id = $request->input('id');
        $branch_id = $request->input('branch_id');

        $response = $passe_user_activity->set_assistance($id, $branch_id);

        return response()->json([
            "error" => "",
            "data" => $response,
        ]);
    }
}
